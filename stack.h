#define MAX 128
typedef struct stack{
	int a[MAX];
	int k;
}stack;
typedef struct stack1{
	char b[MAX];
	int j;
}stack1;
void push(stack *s, int num);
int pop(stack *s);
int empty(stack *s);
int full(stack *s);
void init(stack *s);
void push1(stack1 *s, char c);
int pop1(stack1 *s);
int empty1(stack1 *s);
int full1(stack1 *s);
void init1(stack1 *s);

